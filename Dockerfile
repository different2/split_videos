FROM python:3.7.4-alpine3.10

LABEL maintainer="Rafael Abuzyarov <abuzyarov@adv.ru>"

ENV PYTHONUNBUFFERED 1

COPY ["requirements.txt", "/app/"]
WORKDIR /app

RUN apk add --update --no-cache --virtual .build-deps \
        build-base \
        libffi-dev \
        pcre \
        gcc \
        musl-dev \
        linux-headers \
        make \
    && rm -rf /var/cache/apk/* \
    && pip install --no-cache-dir --upgrade pip==20.0.2 pip-tools==5.1.0 \
    && pip-sync requirements.txt \
    && apk del .build-deps \
    && rm -rf /root/.cache/*

SERVICE = web
TEST_SERVICE = web_test

ifneq ("$(wildcard ./docker-compose.override.yml)","")
    DC_FILE := docker-compose.override.yml
else
    DC_FILE := docker-compose.yml
endif

DC_CMD = docker-compose -f ${DC_FILE}

ifeq ($(name),)
	TEST_CMD := $(DC_CMD) run --rm $(TEST_SERVICE) python -m pytest --cov-report term:skip-covered --cov-report html --no-cov-on-fail --cov
else
	TEST_CMD := $(DC_CMD) run --rm $(TEST_SERVICE) python -m pytest /app/src/$(name)
endif


.PHONY: start stop status restart cli tail build run test pip-compile


help:
	@echo ""
	@echo "Please use \`make <target>' where <target> is one of"
	@echo ""
	@echo "  build              to make all docker assembly images"
	@echo "  test               to run tests"
	@echo "  pip-compile        to make pip-compile"
	@echo ""
	@echo "See contents of Makefile for more targets."

start:
	$(DC_CMD) up -d

stop:
	$(DC_CMD) down

status:
	$(DC_CMD) ps

restart: stop start

cli:
	$(DC_CMD) run --rm $(SERVICE) sh

tail:
	$(DC_CMD) logs -f $(SERVICE)

build:
	$(DC_CMD) build

up:
	$(DC_CMD) up

test:
	$(TEST_CMD)

pip-compile:
	$(DC_CMD) run --rm --no-deps $(SERVICE) pip-compile

